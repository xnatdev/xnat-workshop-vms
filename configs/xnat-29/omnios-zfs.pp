# This Puppet manifest installs NRG's OZMT scripts and other elements for ZFS operations under OmniOS/Illumos.
# This manifest was created in support of XNAT Workshop 2016 for demonstration purposes.  No ongoing
# support for this manifest and included modules can be offered.

Exec {
  path => '/usr/bin:/usr/sbin:/sbin'
}

# Stock .profile for vagrant mucks with things
file {
  '/export/home/vagrant/.profile':
    ensure => absent;
}

# Some desired packages
package {
  ['network/netcat', 'network/rsync', 'system/network/ipqos']:
    ensure => present;
} 

# Message of the day
class { '::motd':
  motd_header => [ "Welcome to ${::fqdn}. This machine is was configured by puppet. OS: ${::operatingsystem} ${::operatingsystemrelease}",
                   "You can examine the deployment log in /var/log/puppet_deploy.log." ]
}

# Nrgillumos module for general OS config
class { '::nrgillumos':
  timezone => 'US/Central',
  profile_path => '/usr/gnu/bin:/opt/csw/bin:/opt/csw/sbin:/opt/csw/gnu:/usr/gnu/bin:/usr/bin:/usr/sbin:/sbin:/bin:/usr/local/bin',
  vim_version => '74',
  postfix_root_alias => 'vagrant',
}  

# Replace with SunSSH with OpenSSH
class { '::nrgillumos::openssh': }

# Now install OMZT and other ZFS support
class { 'ozmt':
  email_to => 'vagrant@localhost',
}
class { 'nrgzfs':
  install_santools => false,
  install_rsf_1 => false,
  tuning_exec_data => {
    'set_nfs_servers' => {
      'command' => 'sharectl set -p servers=512 nfs',
      'onlyif' => "test `sharectl get -p servers nfs | cut -d '=' -f 2` -ne 512",
    },
  },
}
Class['Nrgillumos'] -> Class['Nrgzfs']

# Update these packages last prior to reboot; they create new boot environments
package {
  ['library/security/openssl', 'web/ca-bundle']:
    ensure => latest
}

