# Class: nrgzfs::samba_from_source
# ================================
#
# Private class to handle building Samba from source
#
# Parameters
# ----------
#
# Parameters for this module.
#
# NONE
#
# Variables
# ----------
#
# Variables used by this module.
#
# NONE
#
# Examples
# --------
#
# @example
#    class { '::nrgzfs::opencsw_packages': }
#
# Authors
# -------
#
# Chip Schweiss <chip.schweiss@wustl.edu>
#
# Copyright
# ---------
#
# Copyright 2016 Chip Schweiss, unless otherwise noted.
#

class nrgzfs::samba_from_source {

  # Require class definition
  include ::nrgzfs

  # Do OS tuning
  case $::osfamily {
    'Solaris' : {
      case $::operatingsystem {
        'OmniOS' : {

          # Note that some packages already installed by nrgillumos and possibly profile modules
          $build_pkgs = [ 'developer/build/autoconf',
                          'developer/build/automake',
                          'developer/object-file',
                          'developer/linker',
                          'library/idnkit',
                          'library/idnkit/header-idnkit',
                          'system/library/math',
                          'library/libffi' ]

          $build_opencsw_pkgs = [ 'CSWcurl',
                              'CSWpysetuptools',
                              'CSWpython27-dev',
                              'CSWpkgconfig',
                              'CSWlibp11kit-dev',
                              'CSWgnutls'
                              'CSWlibnss-dev' ]

          package {
            $build_pkgs:
              ensure   => present;
            $build_opencsw_pkgs:
              ensure   => present,
              provider => $::nrgzfs::opencsw_pkg_provider;
          }

          # # TODO: Revisit this later if needed.
          # exec {
          #   'copy_cacerts_curl':
          #     command => 'cat /etc/ssl/cacert.pem > /etc/curl/curlCA',
          #     creates => '/etc/curl/curlCA',
          #     require => File['/etc/curl'],
          # }



        }
        default : {
            fail("Unsupported os: ${::operatingsystem}")
        }
      } # case $::operatingsystem
    }
    default: {
      fail("Unsupported osfamily: ${::osfamily}")
    }
  } # case $::osfamily
  
}
