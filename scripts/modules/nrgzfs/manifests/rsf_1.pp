# Class: nrgzfs::rsf_1
# ===============================
#
# This classes handle RSF-1 installation & config for nrgzfs module.  This class assumes
# OZMT has been installed to /opt/ozmt.
#
# TODO: How to deal wtih RSF-1 re-installation or upgrade.
# TODO: Handle OS's besides OmniOS
#
# Parameters
# ----------
#
# * `rsf_1_fetch_url`
#  Required, the URL from which to fetch the RSF-1 package file.
#
# * `rsf_1_md5`
#  Required, MD5 sum for the pkg file specified in $rsf_1_fetch_url.
#
# * `prop_scsi2_drive_count`
#  Required, the value to use for "rsfcdb update prop_scsi2_drive_count X", default 4.
#
# * `rsf_1_licenses`
#  Optional, a hash of RSF-1 license strings, keyed by $::hostname.  I.e. this assumes
#  the value this node resolves as $::hostname will match the 'hostname' hash key provided.
#  The license string retrieved for the local $::hostname will be written to
#  /opt/HAC/RSF-1/etc/license.${::uniqueid}, where $::uniqueid is the node's host ID. 
#  If no license for my $::hostname is found, no license file written.  See example below.
#  Default: undef.
#
# # `rsf_1_passwds`
#  Optional, a hash of entries to append to /opt/HAC/RSF-1/etc/passwd, keyed by username.  Note
#  that RSF-1 passwd hashes are created using the rsfpasswd tool.  This module doesn't compute
#  hashes.  If this param is undef that file will be ignored.  See example below.
#
# # `rsf_1_passwd_config`
#  Optional, a string representing contents of the file /opt/HAC/RSF-1/etc/passwd.config.  If
#  undef, that file will be ignored.
#
# * `rsfcdb_bootstrap_config_file_contents`
#  Optional, the literal contents to place in the file whose path is specified below in 
#  `rsfcdb_bootstrap_config_file`.  Plase note usage of this parameter is mutually exclusive
#  from using the `rsfcdb_bootstrap_config_cmds` param.  If this param is defined, it will take
#  precendence over any contents of `rsfcdb_bootstrap_config_cmds`.
#
# * `rsfcdb_bootstrap_config_cmds`
#  Optional, an array of rsfcdb {ga_*, sa_*, ha_*} commands to issue for setting configuration
#  values in the local RSF-1 database and then export to the file specified in
#  `rsfcdb_bootstrap_config_file`.  These commands will be issued in the order specified in the
#  array, and be aware they will modify the machine's local RSF-1 config database.  Note this
#  parameter isn't intended to do ongoing operational updates to RSF-1, and it is mutually
#  exclusive with the `rsfcdb_bootstrap_config_file_contents` param above.
#
#  These rsfcdb commands *will not* be run if the file specified in `rsfcdb_bootstrap_config_file`
#  below exists.  I.e., by default these commands will be executed only once upon initial puppet
#  agent run, and only if `rsfcdb_bootstrap_config_file_contents` above is undefined.  Finally,
#  unless rsfcdb is already in the system path prior to RSF-1 package installation, the commands
#  in this array will need to use full path /opt/HAC/RSF-1/bin/rsfcdb.
#
#  Please refer to RSF-1 documentation on rsfcdb command syntax (and the example below):
#  http://www.high-availability.com/wp-content/uploads/2014/04/rsfcdb-configuration-quickstart.pdf
#
# * `rsfcdb_bootstrap_config_file`
#  The path of a config file to write the contents of `rsfcdb_bootstrap_config_file_contents` to,
#  OR for rsfcdb to export to after running the commands specified in
#  `rsfcdb_bootstrap_config_cmds`, depending on which parameter is used.  This config file would 
#  then be useful for manual distribution to other ZFS nodes.  This parameter has no effect if 
#  `rsfcdb_bootstrap_config_file_contents` and `rsfcdb_bootstrap_config_cmds` are both undef.
#  Default value /opt/HAC/RSF-1/etc/config.puppet .
#
# * `vlans`
#  Optional, a hash of VLANs to create for network layer failover.  For OmniOS these VLANs are 
#  created with dladm create-vlan.  See example below.
#
# * `ipmp_groups`
#  Optional, hash of IPMP groups to create for failover, including a list of interfaces within
#  each group.  Note the interfaces can be VLANs specififed in `vlans` above.  See example below.
#
# Variables
# ----------
#
# Variables used by this module.
#
# * `uniqueid`
#  This module will resolve the machine's host ID via facter $::uniqueid
#
# Examples
# --------
#
# @example
#    class { '::nrgzfs::rsf_1':
#      rsf_1_fetch_url => 'ftp://ftp.high-availability.com/solaris/rsf-1-solaris-5.11-x86.3.9.10.2014-12-04.pkg',
#      rsf_1_md5 => 'b515f1dbe088b753f1df22121544ba77',
#      rsf_1_licenses => {
#         'my-nodes-hostname' => 'LICENSE_CONTENT_BLAH_BLAH',
#      },
#      rsf_1_passwds => {
#         'root' => 'ROOT_RSF-1_PASSWD_HASH_BLAH_BLAH',
#      },
#      rsf_1_passwd_config => 'RSF-1_PASSWD_CONFIG_CONTENT_BLAH_BLAH',
#      },
#      rsf_bootstrap_config_cmds = [
#        '/opt/HAC/RSF-1/bin/rsfcdb ga_name My_Cluster',
#        '/opt/HAC/RSF-1/bin/rsfcdb rsfcdb ga_bo 20,3,3600',
#        '/opt/HAC/RSF-1/bin/rsfcdb rsfcdb ga_pt 1',
#        '/opt/HAC/RSF-1/bin/rsfcdb rsfcdb ga_rt 1',
#        '/opt/HAC/RSF-1/bin/rsfcdb rsfcdb ga_ipd 3,2',
#      ],
#      vlans => {
#        'vlan500v0' => {
#          'link' => 'hme0',
#          'vid' => '500',
#          'vlan-link' => 'vlan500v0',
#        },
#        'vlan500v1' => {
#          'link' => 'hme1',
#          'vid' => '500',
#          'vlan-link' => 'vlan500v1',
#        },
#      },
#      ipmp_groups => {
#        'vlan500i0' => {
#          'group_name' => 'vlan500i0',
#          'data_address' => '0.0.0.0/0',
#          'underlying_ifs' => [ 'vlan500v0', 'vlan500v1' ],
#        }
#      }
#    }
#
# Authors
# -------
#
# Chip Schweiss <chip.schweiss@wustl.edu>
#
# Copyright
# ---------
#
# Copyright 2016 Chip Schweiss, unless otherwise noted.
#

class nrgzfs::rsf_1 (
  String $rsf_1_fetch_url,
  String $rsf_1_md5,
  Integer $prop_scsi2_drive_count = 4,
  String $rsfcdb_bootstrap_config_file = '/opt/HAC/RSF-1/etc/config.puppet',
  Optional[Hash] $rsf_1_licenses = undef,
  Optional[Hash] $rsf_1_passwds = undef,
  Optional[String] $rsf_1_passwd_config = undef,
  Optional[String] $rsfcdb_bootstrap_config_file_contents = undef,
  Optional[Array] $rsfcdb_bootstrap_config_cmds = undef,
  Optional[Hash] $vlans = undef,
  Optional[Hash] $ipmp_groups = undef,
  ) {

  include '::wget'
  require '::ozmt'

  # Create any VLANs specified
  $vlans.each |$v, $vlan| {
    if ($vlan['link'] and $vlan['vid'] and $vlan['vlan-link']) {
      exec { "create-vlan ${vlan[vlan-link]} over ${vlan[link]}":
        command => "dladm create-vlan -l ${vlan[link]} -v ${vlan[vid]} ${vlan[vlan-link]} ; \
        ifconfig ${vlan[vlan-link]} plumb up",
        unless => "dladm show-vlan | grep ${vlan[vlan-link]}",
      }
    }
  }
  # Now create any IPMP groups specified
  $ipmp_groups.each |$i, $ipmp_group| {
    if ($ipmp_group['group_name'] and $ipmp_group['data_address']) {
      exec { "create ipmp_group ${ipmp_group[group_name]}":
        command => "ifconfig ${ipmp_group[group_name]} ipmp ${ipmp_group[data_address]} up ; \
        echo \"ipmp group ${ipmp_group[group_name]} ${ipmp_group[data_address]} up\" > /etc/hostname.${ipmp_group[group_name]}",
        creates => "/etc/hostname.${ipmp_group[group_name]}",
      }
      $ipmp_group['underlying_ifs'].each |$if| {
        exec { "${if} failover for ${ipmp_group[group_name]}":
          command => "ifconfig ${if} -failover group ${ipmp_group[group_name]} up ; \
          echo \"group ${ipmp_group[group_name]} -failover up\" > /etc/hostname.${if}",
          creates => "/etc/hostname.${if}",
        }
      }
    }
  } 

  # Need this to coax maestrodev/wget to work under Solaris
  if (! defined(Package['wget']) ) {
    package { 'wget':
      ensure => present,
    }
  }
  
  $rsf_1_dest = '/opt/rsf_1_puppet/' # Trailing slash is required
  $rsf_1_pkg_filename = basename($rsf_1_fetch_url)
  
  # Retrieve the RSF-1 package file, but only if not already downloaded
  wget::fetch { 'rsf_1_pkg':
    source => $rsf_1_fetch_url,
    source_hash => $rsf_1_md5,
    destination => $rsf_1_dest,
    unless => "test -f ${rsf_1_dest}/${rsf_1_pkg_filename}",
    notify => Package['rsf-1'],
  }
  File[$rsf_1_dest] -> Wget::Fetch['rsf_1_pkg']

  # pkgadd needs its silly admin file
  file { '/tmp/pkgadd_rsf-1' :
    ensure => present,
    content => "mailinstance=overwrite
partial=nocheck
runlevel=nocheck
idepend=nocheck
rdepend=nocheck
space=nocheck
setuid=nocheck
conflict=nocheck
action=nocheck
basedir=default",
  }
  
  # Install package file downloaded.  Use 'sun' provider to get pkgadd tool.
  package { 'rsf-1':
    provider => 'sun',
    install_options => [ '-a/tmp/pkgadd_rsf-1' ],
    ensure => present,
    source => "${rsf_1_dest}/${rsf_1_pkg_filename}",
    require => [ Wget::Fetch['rsf_1_pkg'], File['/tmp/pkgadd_rsf-1'] ],
    notify => [ Exec['patch /opt/HAC/bin/rsf.sh'],
                Exec['patch /opt/HAC/RSF-1/etc/service/disks/S20zfs'],
                Exec['patch /opt/HAC/RSF-1/etc/rc.appliance.c/S20zfs'],
                Exec['prop_comstar_support'],
                Exec['prop_zpool_sync_cache'],
                Exec['prop_use_zfs_cache'],
                Exec['prop_zpool_import_abort_all_errors'],
                Exec['prop_zpool_cache_detach'],
                Exec['prop_zpool_import_command'],
                Exec['prop_zpool_cache_attach'],
                Exec['prop_zpool_export_command'],
                Exec['prop_scsi2_retry_count'],
                Exec['prop_scsi2_threaded_reserve'],
                Exec['prop_scsi2_drive_count'] ];
  }

  # Patch some files deposited by the rsf-1 package
  exec {
    'patch /opt/HAC/bin/rsf.sh':
      command => "patch -p1 < ${rsf_1_dest}/rsf.sh.patch",
      cwd => '/',
      require => File["${rsf_1_dest}/rsf.sh.patch"],
      unless => "grep 'Chip Schweiss' /opt/HAC/bin/rsf.sh",
      refreshonly => true;
    'patch /opt/HAC/RSF-1/etc/service/disks/S20zfs':
      command => "patch -p6 < ${rsf_1_dest}/S20zfs.patch",
      cwd => '/opt/HAC/RSF-1/etc/service/disks/',
      require => File["${rsf_1_dest}/S20zfs.patch"],
      unless => "grep 'Chip Schweiss' /opt/HAC/RSF-1/etc/service/disks/S20zfs",
      refreshonly => true;
    'patch /opt/HAC/RSF-1/etc/rc.appliance.c/S20zfs':
      command => "patch -p6 < ${rsf_1_dest}/S20zfs.patch",
      cwd => '/opt/HAC/RSF-1/etc/rc.appliance.c/',
      require => File["${rsf_1_dest}/S20zfs.patch"],
      unless => "grep 'Chip Schweiss' /opt/HAC/RSF-1/etc/rc.appliance.c/S20zfs",
      refreshonly => true;
  }

  # Retrieve license content for my hostname, if available, and write to file
  if $rsf_1_licenses and $rsf_1_licenses["${::hostname}"] {
    $license_file ="/opt/HAC/RSF-1/etc/license.${::uniqueid}"
    file { $license_file:
      content => $rsf_1_licenses["${::hostname}"],
      require => Package['rsf-1'],
    }
  }

  # Retrieve passwd content, if available, and write to file
  if $rsf_1_passwds {
    $rsf_1_passwds.each |$uname, $val| {
      file_line { "/opt/HAC/RSF-1/etc/passwd ${uname}":
        path => '/opt/HAC/RSF-1/etc/passwd',
        line => "${uname}:${val}",
        match => "${uname}:.*",
        replace => true,
        multiple => true,
        require => Package['rsf-1'],
      }
    }
  }
  if $rsf_1_passwd_config {
    file { '/opt/HAC/RSF-1/etc/passwd.config':
      content => $rsf_1_passwd_config,
      require => Package['rsf-1'],
    }
  }

  # Declare other file resources
  file {
    $rsf_1_dest:
      ensure => directory;
    "${rsf_1_dest}/rsf.sh.patch":
      ensure => present,
      source => 'puppet:///modules/nrgzfs/RSF-1/rsf.sh.patch';
    "${rsf_1_dest}/S20zfs.patch":
      ensure => present,
      source => 'puppet:///modules/nrgzfs/RSF-1/S20zfs.patch';
  }

  # Set some rsfcdb properties
  exec {
    'prop_comstar_support':
      command => '/opt/HAC/RSF-1/bin/rsfcdb update prop_comstar_support false',
      refreshonly => true,
      unless => '/opt/HAC/RSF-1/bin/rsfcdb read prop_comstar_support | grep false';
    'prop_zpool_sync_cache':
      command => '/opt/HAC/RSF-1/bin/rsfcdb update prop_zpool_sync_cache false',
      refreshonly => true,
      unless => '/opt/HAC/RSF-1/bin/rsfcdb read prop_zpool_sync_cache | grep false';
    'prop_use_zfs_cache':
      command => '/opt/HAC/RSF-1/bin/rsfcdb update prop_use_zfs_cache true',
      refreshonly => true,
      unless => '/opt/HAC/RSF-1/bin/rsfcdb read prop_use_zfs_cache | grep true';
    'prop_zpool_import_abort_all_errors':
      command => '/opt/HAC/RSF-1/bin/rsfcdb update prop_zpool_import_abort_all_errors false',
      refreshonly => true,
      unless => '/opt/HAC/RSF-1/bin/rsfcdb read prop_zpool_import_abort_all_errors | grep false';
    'prop_zpool_cache_detach':
      command => '/opt/HAC/RSF-1/bin/rsfcdb update prop_zpool_cache_detach /opt/ozmt/utils/zpool-cache-detach.sh',
      refreshonly => true,
      unless => '/opt/HAC/RSF-1/bin/rsfcdb read prop_zpool_cache_detach | grep \'/opt/ozmt/utils/zpool-cache-detach.sh\'';
    'prop_zpool_import_command':
      command => '/opt/HAC/RSF-1/bin/rsfcdb update prop_zpool_import_command /opt/ozmt/utils/fast-zpool-import.sh',
      refreshonly => true,
      unless => '/opt/HAC/RSF-1/bin/rsfcdb read prop_zpool_import_command | grep \'/opt/ozmt/utils/fast-zpool-import.sh\'';
    'prop_zpool_cache_attach':
      command => '/opt/HAC/RSF-1/bin/rsfcdb update prop_zpool_cache_attach /opt/ozmt/utils/zpool-cache-attach.sh',
      refreshonly => true,
      unless => '/opt/HAC/RSF-1/bin/rsfcdb read prop_zpool_cache_attach | grep \'/opt/ozmt/utils/zpool-cache-attach.sh\'';
    'prop_zpool_export_command':
      command => '/opt/HAC/RSF-1/bin/rsfcdb update prop_zpool_export_command /opt/ozmt/utils/fast-zpool-export.sh',
      refreshonly => true,
      unless => '/opt/HAC/RSF-1/bin/rsfcdb read prop_zpool_export_command | grep \'/opt/ozmt/utils/fast-zpool-export.sh\'';
    'prop_scsi2_retry_count':
      command => "/opt/HAC/RSF-1/bin/rsfcdb update prop_scsi2_retry_count 3",
      refreshonly => true,
      onlyif => "test `/opt/HAC/RSF-1/bin/rsfcdb read prop_scsi2_retry_count` -ne 3";
    'prop_scsi2_threaded_reserve':
      command => '/opt/HAC/RSF-1/bin/rsfcdb update prop_scsi2_threaded_reserve true',
      refreshonly => true,
      unless => '/opt/HAC/RSF-1/bin/rsfcdb read prop_scsi2_threaded_reserve | grep true';
    'prop_scsi2_drive_count':
      command => "/opt/HAC/RSF-1/bin/rsfcdb update prop_scsi2_drive_count ${prop_scsi2_drive_count}",
      refreshonly => true,
      onlyif => "test `/opt/HAC/RSF-1/bin/rsfcdb read prop_scsi2_drive_count` -ne ${prop_scsi2_drive_count}";
  }

  # If RSF-1 config file contents are specified, dump them to file
  if $rsfcdb_bootstrap_config_file_contents {
    file { $rsfcdb_bootstrap_config_file:
      content => "# This file is generated by Puppet.  Don't delete unless you know what you're doing!\n${rsfcdb_bootstrap_config_file_contents}",
      require => Package['rsf-1'],
    }
  }
  # Else, if rsfcdb bootstrap config commands specified, update the database and export config file
  elsif $rsfcdb_bootstrap_config_cmds {
    $rsfcdb_bootstrap_config_cmds_real = concat($rsfcdb_bootstrap_config_cmds,
    "/opt/HAC/RSF-1/bin/rsfcdb config_make ${rsfcdb_bootstrap_config_file}")
    $rsfcdb_bootstrap_config_cmds_real_string = join($rsfcdb_bootstrap_config_cmds_real, ' ; ')
    exec { 'rsfcdb_bootstrap_config' :
      command => $rsfcdb_bootstrap_config_cmds_real_string,
      creates => $rsfcdb_bootstrap_config_file,
      require => Package['rsf-1'],
      notify => File_line["${rsfcdb_bootstrap_config_file}"],
    }
    file_line { $rsfcdb_bootstrap_config_file:
      path => $rsfcdb_bootstrap_config_file,
      match => '^# This file is automatically.*$',
      line => '# This file is generated by rsfcdb+puppet.  Don\'t delete unless you know what you\'re doing!',
    }
  }
}
