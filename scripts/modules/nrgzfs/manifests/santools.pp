# Class: nrgzfs::santools
# ===========================
#
# This class installs Santools.
#
# Parameters
# ----------
#
# * `santools_fetch_url`
#  Required, the URL from which to fetch the gzipped santools tarball.  This tarball must
#  contain the files smartmon-ux, santoolszdb, and smartmon-ux.conf in the archive's root
#  directory.
#
# * `santools_md5`
#  Required, MD5 sum for the tarball specified in $santools_fetch_url.
#
# * `santools_licenses`
#  Optional, a hash of Santools license strings, keyed by $::hostname.  I.e. this assumes
#  the value this node resolves as $::hostname will match the 'hostname' hash key provided.
#  If no license for my  $::hostname is found, this module does nothing.  See example below.
#  Default: undef.
#
# Variables
# ----------
#
# NONE
#
# Examples
# --------
#
# @example
#    class { '::nrgzfs::santools':
#      santools_fetch_url => 'https://somewhere.com/santools/santools-1.0.tar.gz',
#      santools_md5 => 'ebcb73f29d30a90df6d72dec554a7de7',
#      santools_licenses => {
#         'my-nodes-hostname' => 'LICENSE_CONTENT_BLAH_BLAH',
#       }
#    }
#
# Authors
# -------
#
# Chip Schweiss <chip.schweiss@wustl.edu>
#
# Copyright
# ---------
#
# Copyright 2016 Chip Schweiss, unless otherwise noted.
#

class nrgzfs::santools (
  String $santools_fetch_url,
  String $santools_md5,
  Optional[Hash] $santools_licenses = undef,
) {

  include '::wget'

  # Need this to coax maestrodev/wget to work under Solaris
  if (! defined(Package['wget']) ) {
    package { 'wget':
      ensure => present,
    }
  }

  $santools_dest = '/opt/santools_puppet/' # Trailing slash is required
  $tarball_filename = basename($santools_fetch_url)

  # Retrieve license content for my hostname, if available
  if $santools_licenses and $santools_licenses["${::hostname}"] {
     $license_content = $santools_licenses["${::hostname}"]
  }
  else {
    $license_content = undef
  }

  # Install Santools if a license is available
  if $license_content {
    # Note this wget transfer only occurs when tarball specified not already downloaded
    wget::fetch { 'santools_tarball':
      source => $santools_fetch_url,
      source_hash => $santools_md5,
      destination => $santools_dest,
      unless => "test -f ${santools_dest}/${tarball_filename}",
      notify => Exec['santools_untar'],
    }
    File[$santools_dest] -> Wget::Fetch['santools_tarball']

    # Unpack the santools archive (assumes tar and gzip are installed)
    exec { 'santools_untar':
      command => "tar xfz ${santools_dest}/${tarball_filename}",
      cwd => "${santools_dest}/unpacked",
      refreshonly => true,
      require => File["${santools_dest}/unpacked"],
      notify => Exec['santools_chmod'],
    }

    # Chmod the santools binaries, failing if files missing (desired behavior).
    exec { 'santools_chmod':
      command => "chmod 755 ${santools_dest}/unpacked/smartmon-ux ${santools_dest}/unpacked/santoolszdb",
      refreshonly => true,
    }

    # Now declare file resources. Links will fail if target missing (desired behavior).
    file {
      $santools_dest:
        ensure => directory;
      "${santools_dest}/unpacked":
        ensure => directory,
        require => File[$santools_dest];
      '/sbin/smartmon-ux':
        ensure  => link,
        target => "${santools_dest}/unpacked/smartmon-ux",
        require => Exec['santools_untar'];
      '/usr/sbin/santoolszdb':
        ensure  => link,
        target => "${santools_dest}/unpacked/santoolszdb",
        require => Exec['santools_untar'];
      '/etc/smartmon-ux.conf':
        ensure => present,
        source => "puppet:///modules/nrgzfs/smartmon-ux.conf";
      '/etc/.smartmon-uxlicense.txt':
        mode    => '644',
        content => "${license_content}",
        replace => true;
    }
  } 
}
