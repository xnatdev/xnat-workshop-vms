# Class: nrgzfs::private::packages
# ===============================
#
# Private class to handle prerequisite package installation for nrgzfs module.
#
# Parameters
# ----------
#
# Parameters for this module.
#
# NONE
#
# Variables
# ----------
#
# Variables used by this module.
#
# * `nrgzfs::opencsw_pkg_provider`
#  The package provider to use when installing OpenCSW packages.  Default: pkgutil.
#
# Examples
# --------
#
# @example
#    class { '::nrgzfs::private::packages': }
#
# Authors
# -------
#
# Chip Schweiss <chip.schweiss@wustl.edu>
#
# Copyright
# ---------
#
# Copyright 2016 Chip Schweiss, unless otherwise noted.
#

class nrgzfs::private::packages {
  
  # Install prereq packages, based on OS
  case $::osfamily {
    'Solaris' : {
      case $::operatingsystem {
        'OmniOS' : {

          # OmniOS packages
          $omnios_pkg_list = [ 'storage/mpathadm',
                               'system/pciutils/pci.ids',
                               'system/pciutils' ]
          
          # Note CSWmercurial has undocumented dep on CSWpython27
          $opencsw_pkg_list = [ 'CSWcolumn',
                                'CSWpython27',
                                'CSWmercurial',
                                'CSWmosh' ]
          
          package {
            $omnios_pkg_list:
              ensure   => present;
            $opencsw_pkg_list:
              ensure   => present,
              provider => $::nrgzfs::opencsw_pkg_provider;
          }
        }
        default : {
          fail("Unsupported os: ${::operatingsystem}")
        }
      } 
      # case $::operatingsystem
    }
    default: {
      fail("Unsupported osfamily: ${::osfamily}")
    }
  } 
  # case $::osfamily
}
