# Class: nrgzfs::ha
# =============================
#
# This class manages ZFS server High-Availability stuff on NRG
# OmniOS/Illumos machines.  It is assumed this class is invoked in conjunction with
# nrgzfs::init.
#
# Parameters
# ----------
#
# Below are the parameters this class accepts
#
# TBD
#
# Variables
# ----------
#
# Variables that this module requires.
#
# TBD
#
# Examples
# --------
#
# @example
#    class { '::nrgzfs::ha':
#      some_parameter = 'blahblah',
#    }
#
# Authors
# -------
#
# Author Name <author@domain.com>
#
# Copyright
# ---------
#
# Copyright 2015 Your name here, unless otherwise noted.
#
class nrgzfs::ha {

  # Include base class
  include ::nrgzfs
  # Now do HA stuff

}


