# Class: nrgillumos
# ===========================
#
# This the base class for nrgillumos module, to manage Illumos/OmniOS config that is
# shared by all NRG machines.  This class (and the module) is partly intended to provide
# some OmniOS compatibility with 3rd party modules used on Linux machines, i.e. so that
# parameters specified for timezone, ssh, puppet-agent, etc on Linux can also be applied
# to OmniOS.
#
# With the exception of nrgillumous::puppet_agent, all public classes in this 
# module include this base class.  The nrgillumous::puppet_agent class is intended to be
# invoked separately to satifsy class ordering requirements.
#
# Parameters
# ----------
#
# Below are the parameters this class accepts
#
# * 'pkg_publishers'
#  An array of publishers to use for pkg, default ['http://pkg.omniti.com/omnios/r151014/ omnios',
#  'http://pkg.omniti.com/omniti-ms/ ms.omniti.com']
#
# * `opencsw_pkg_provider`
#  The 'provider' attribute to use for installing OpenCSW packages, default 'pkgutil'
#
# * `postfix_pkg_provider`
#  The 'provider' attribute to use for postfix package, default 'pkgutil'
#
# * `postfix_pkg_name`
#  The name of the postfix package to install, default 'CSWpostfix'
#
# * `postfix_service_name`
#  The name of the postifx service created by the package above, default 'cswpostfix'
#
# * `postfix_config_directory`
#  The config directory used by postfix, default '/etc/opt/csw/postfix'
#
# * `postfix_service_enabled`
#  Whether the postifx service should be enbaled, default true
#
# * `postfix_root_alias`
#  Optional, a mail alias to specify for root, default undef (i.e. no alias)
#
# * `postfix_relay_host`
#  Optional, the relay host that the local postfix server will use.  Note you can (and
#  probably should) use the syntax "[mailhost.domain.com]".  Default undef (no host).
#
# * `profile_path`
#  Required, the system PATH to put in /etc/profile for console sessions, default:
#  /usr/gnu/bin:/opt/csw/bin:/opt/csw/sbin:/opt/csw/gnu:/usr/gnu/bin:/usr/bin:/usr/sbin:/sbin:/bin:/usr/local/bin
#
# * `timezone`
#  Optional, the timezone to set in /etc/default/init, default undef
#
# * `vim_version`
#  Required, a version of vim to specify when marking up filetype.vim, default '74'.
#
# * `opencsw_mirror`
#  Optional, an OpenCSW mirror URL to put in /etc/opt/csw/pkgutil.conf, default undef
#
# * `hostids`
#  Optional, a hash of Host IDs, keyed by $::hostname.  This is the ID the machine
#  resolves via facter $::uniqueid, and this module will update that ID to any value that
#  resolves for $hostids[$::hostname].  Note that Host ID change requires a reboot, which
#  this module doesn't do.
#
# * `setnotify_exec_data`
#  A hash to pass as $data to create_resources('exec', $data, $defaults), for configuring SMF
#  notificiations.  Default:
#  'global_notify' => {
#     'command' => 'svccfg -s svc:/system/svc/global:default setnotify -g from-online  mailto:root@localhost',
#     'unless' => 'svccfg -s svc:/system/svc/global:default listnotify | grep root@localhost',
#   },
#
# * `setnotify_exec_defaults`
#  A hash to pass as $defaults to create_resources('exec', $data, $defaults), to configuring SMF
#  notifications.  Default undef.
#
# Possibly useful reference:
# https://github.com/rgevaert/puppet-postfix (supports Solaris)
#
# Variables
# ----------
#
# Variables that this class requires.
#
# * `uniqueid`
#  This class will resolve the machine's host ID via facter $::uniqueid
#
# Examples
# --------
#
# @example
#    class { '::nrgillumos':
#      opencsw_pkg_provider = 'pkgutil',
#      postfix_pkg_name = 'postfix',
#      postfix_service_name = 'cswpostfix',
#      postfix_config_directory = '/etc/opt/csw/postfix',
#    }
#
# Authors
# -------
#
# Author Name <author@domain.com>
#
# Copyright
# ---------
#
# Copyright 2015 Your name here, unless otherwise noted.
#
class nrgillumos (
  Array $pkg_publishers = ['http://pkg.omniti.com/omnios/r151014/ omnios',
                           'http://pkg.omniti.com/omniti-ms/ ms.omniti.com'],
  String $opencsw_pkg_provider = 'pkgutil',
  String $postfix_pkg_provider = 'pkgutil',
  String $postfix_pkg_name = 'CSWpostfix',
  String $postfix_service_name = 'cswpostfix',
  String $postfix_config_directory = '/etc/opt/csw/postfix',
  Boolean $postfix_service_enabled = true,
  String $profile_path = '/usr/gnu/bin:/opt/csw/bin:/opt/csw/sbin:/opt/csw/gnu:/usr/gnu/bin:/usr/bin:/usr/sbin:/sbin:/bin:/usr/local/bin',
  Variant[Numeric, String] $vim_version = '74',
  Optional[String] $postfix_root_alias = undef,
  Optional[String] $postfix_relay_host = undef,
  Optional[String] $timezone = undef,
  Optional[String] $opencsw_mirror = undef,
  Optional[Hash] $hostids = undef,
  Optional[Hash] $setnotify_exec_data = {
    'global_notify' => {
      'command' => 'svccfg -s svc:/system/svc/global:default setnotify -g from-online  mailto:root@localhost',
      'unless' => 'svccfg -s svc:/system/svc/global:default listnotify | grep root@localhost',
    },
  },
  Optional[Hash] $setnotify_exec_defaults = undef,
) {
  
  # Set pkg publishers
  $pkg_publishers.each |$publisher| {
    $fields = split($publisher, ' ')
    exec { "set pkg publisher ${fields[0]}":
      command => "pkg set-publisher -g ${publisher}",
      unless => "pkg publisher | grep '${fields[0]}'",
    }
  }

  # Update OpenCSW mirror, assumes pkgutil already installed
  if $opencsw_mirror {
    file_line { 'nrgillumos_opencsw_mirror':
      path => '/etc/opt/csw/pkgutil.conf',
      line => "mirror=${opencsw_mirror}",
      match => '^mirror=.*',
    }
    File_line['nrgillumos_opencsw_mirror'] -> Class['Nrgillumos::Private::Opencsw_packages']
    File_line['nrgillumos_opencsw_mirror'] -> Class['Nrgillumos::Private::Postfix']
  }

  # Set timezone
  if $timezone {
    file_line { 'nrgillumos_timezone':
      path => '/etc/default/init',
      line => "TZ=\"${timezone}\"",
      match => '^TZ=.*',
    }
  }

  # Include private classes
  include ::nrgillumos::private::packages
  include ::nrgillumos::private::opencsw_packages
  include ::nrgillumos::private::postfix
  include ::nrgillumos::private::vim
  include ::nrgillumos::private::notify

  # Populate /etc files
  $puppetmotd_path = "${::settings::confdir}/puppetmotd"
  file {
    '/etc/inputrc':
      mode => '644',
      source => 'puppet:///modules/nrgillumos/inputrc';
    '/etc/lesskey.input':
      mode => '644',
      source => 'puppet:///modules/nrgillumos/lesskey.input';
    '/etc/profile':
      mode => '644',
      content => template('nrgillumos/profile.erb');
    '/root/.profile':
      ensure => present,
      notify => [ File_Line['root_profile_comment'], 
                  File_Line['root_profile_path'], 
                  File_Line['root_profile_prompt'] ];
    $puppetmotd_path:
      mode   => '755',
      ensure => directory;
  }

  # Disable any /root/.profile overrides that we don't want
  file_line {
    'root_profile_comment':
      line => '# This file managed by nrg\nrgillumos Puppet module.',
      after => '^# Use less.*',
      path => '/root/.profile';
    'root_profile_path':
      path => '/root/.profile',
      line => '#export PATH=/usr/gnu/bin:/usr/bin:/usr/sbin:/sbin',
      match => '^export PATH=.*',
      replace => true,
      multiple => true;
    'root_profile_prompt':
      path => '/root/.profile',
      line => 'PS1_OLD=\'${LOGNAME}@$(/usr/bin/hostname):$(',
      match => '^PS1=.*',
      replace => true;
  } 

  # Disable fast reboot
  exec {
    'set_fastreboot_default':
      command => 'svccfg -s "system/boot-config:default" setprop config/fastreboot_default=false',
      onlyif => 'test `svccfg -s "system/boot-config:default" listprop config/fastreboot_default | cut -d \' \' -f 5` != false',
      notify => Exec['refresh_boot_config'];
    'set_fastreboot_onpanic':
      command => 'svccfg -s "system/boot-config:default" setprop config/fastreboot_onpanic=false',
      onlyif => 'test `svccfg -s "system/boot-config:default" listprop config/fastreboot_onpanic | cut -d \' \' -f 5` != false',
      notify => Exec['refresh_boot_config'];
    'refresh_boot_config':
      command => 'svcadm refresh svc:/system/boot-config:default',
      refreshonly => true;
  }

  # Set hostid
  if $hostids and $hostids["${::hostname}"] {
    if $hostids["${::hostname}"] != $::uniqueid {
      # Files created in /tmp will get wiped on reboot
      $my_hostid = $hostids["${::hostname}"]
      exec {
        'update_hostid':
          command  => "/tmp/set_hostid.sh \"$my_hostid\" ; \
          echo 'Reboot needed to update hostid' > /tmp/update_hostid_motd.txt ; \
          ln -s /tmp/update_hostid_motd.txt ${puppetmotd_path}/update_hostid",
          user     => 'root',
          creates  => '/tmp/hostid_pending',
          require => File['/tmp/set_hostid.sh', "${puppetmotd_path}"];
      }
      file {
        '/tmp/set_hostid.sh':
          source => 'puppet:///modules/nrgillumos/set_hostid.sh',
          mode   => '700';
      }
    }
    else {
      file {
        "${puppetmotd_path}/update_hostid":
          ensure => absent;
      }
    }
  }
}
