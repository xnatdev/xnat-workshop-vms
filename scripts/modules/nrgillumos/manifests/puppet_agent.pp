# Class: nrgillumos::puppet_agent
# ===============================
#
# Provisional support for managing Puppet agent under OmniOS/Illumos, coded to provide
# some meager compatibility with the parameters also used by theforeman/puppet module. 
# Ideally, a Forge module will eventually render this class obsolete.
#
# This class needs to be declared explicitly, so that it can receive parameters
# and also satisfy class ordering requirements.  This class is not included
# automatically by nrgillumos::init.
#
# Parameters
# ----------
#
# Below are the parameters this class accepts
#
# * `user`
#  The puppet user, default puppet
#
# * `group`
#  The puppet user group, default puppet
#
# * `puppetmaster`
#  The FQDN of the Puppetmaster.  If not specified, this class will remove the
#  "server" parameter from puppet.conf to let the agent use its default value.
#
# * `ca_server`
#  The FQDN of the Puppetmaster CA.  If not specified, this class will remove the
#  "ca_server" parameter from puppet.conf to let the agent use its default value.
#
# * `runinterval`
#  The run interval for puppet agent, in seconds.  If not specified, this class
#  will remove this parameter from puppet.conf to let the agent use its default
#  value.
#
# * `runmode`
#  The mode in which the puppet agent runs, with current values only being "service"
#  and "none".  Specifying "service" causes puppet agent to be installed into SMF and
#  enabled.  Specifying "none" causes any existing service definition for puppet to
#  be removed and any running agent stopped.  Default is 'service'.
#
# * `environment`
#  Default environment of the Puppet agent.  Default is $::environment.
#
# * `dir`
#  The path to puppet.conf file.  Default is '/etc/puppetlabs/puppet'.
#
# * `bindir`
#  The path to 'puppet' executable.  Default is '/opt/csw/bin'.
#
# Examples
# --------
#
# @example
#    class { '::nrgillumos::puppet_agent':
#      puppetmaster => 'puppet.mydomain.com',
#      ca_server => 'puppetca.mydomain.com',
#      runinternval => '3600',
#      runmode => 'service',
#      bindir => '/opt/csw/bin',
#    }
#
# Authors
# -------
#
# Author Name <author@domain.com>
#
# Copyright
# ---------
#
# Copyright 2015 Your name here, unless otherwise noted.
#
class nrgillumos::puppet_agent (
  String $user = 'puppet',
  String $group = 'puppet',
  String $runmode = 'service',
  String $environment = $::environment,
  String $dir = '/etc/puppetlabs/puppet',
  String $bindir = '/opt/csw/bin',
  Optional[String] $puppetmaster = undef,
  Optional[String] $ca_server = undef,
  Optional[Variant] $runinterval = undef,
  ) {

  # Create puppet user and group
  user { $user:
    home => '/opt/puppetlabs/puppet/cache',
    gid => $group,
    shell => '/usr/gnu/bin/false',
    require => Group["${group}"];
  }
  group { $group:
    ensure => present;
  }

  # Manage puppet.conf
  concat { "${dir}/puppet.conf":
    owner => 'root',
    mode  => '0644',
  }
  concat::fragment { 'puppet.conf+main+agent':
    target  => "${dir}/puppet.conf",
    content => template('nrgillumos/puppet/puppet.conf.erb'),
    order   => '20',
    notify => Service['network/puppet'],
  }
  
  # Create puppet agent service definition in SMF
  if ($runmode == 'service') {
    # Install the method script
    file { 'puppet_smf_method_script_solaris':
      path => '/lib/svc/method/puppet',
      ensure => present,
      mode => '0755',
      owner => 'root',
      group => 'bin',
      content => template('nrgillumos/puppet/svc-puppet.erb'),
      require => Concat::Fragment['puppet.conf+main+agent'],
    }

    # Install SMF manifest and install it
    file { 'puppet_smf_manifest_solaris':
      path => '/lib/svc/manifest/site/puppet.xml',
      ensure => present,
      content => template('nrgillumos/puppet/smf-puppet.xml.erb'),
      require => File['puppet_smf_method_script_solaris'],
    }
    exec { 'puppet_smf_manifest_import_solaris':
      command => 'svcadm restart manifest-import',
      unless => 'svcs -a | grep puppet',
      require => File['puppet_smf_manifest_solaris'],       
    }

    # Define puppet agent service and enable it.
    service {'network/puppet':
      ensure     => running,
      enable     => true,
      require => Exec['puppet_smf_manifest_import_solaris'],
      subscribe => Concat::Fragment['puppet.conf+main+agent'],
    }
  }

  elsif ($runmode == 'none') {
    # TODO: Remove any existing service definition
  }

}
