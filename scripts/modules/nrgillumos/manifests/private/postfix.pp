# This is the private class nrgillumos::private::postfix, and it should not be
# included explicitly.
#
# This class manages postfix, and it is included by nrgillumos::init.
#
class nrgillumos::private::postfix {
  
  # Declare package resource
  package { $::nrgillumos::postfix_pkg_name:
    ensure => installed,
    provider => $::nrgillumos::postfix_pkg_provider,
    notify => Exec['gcp_etc_aliases'],
  }
  
  # Copy over any existing /etc/aliases, if it exists and contains updates
  exec { 'gcp_etc_aliases':
    command => "gcp -u /etc/aliases ${::nrgillumos::postfix_config_directory}/aliases",
    onlyif => 'test -f /etc/aliases',
    refreshonly => true,
  }

  # Add comments to config files managed
  file_line { 'postfix_main.cf_comment':
    path => "${::nrgillumos::postfix_config_directory}/main.cf",
    line => '# Managed by nrg/nrgillumos Puppet module, please be careful about editing.',
    after => '^# list, see the postconf.*',
    require => Package["$::nrgillumos::postfix_pkg_name"],
  }
  file_line { 'postfix_main.cf_alias_maps':
    path => "${::nrgillumos::postfix_config_directory}/main.cf",
    match => '^alias_maps = .*',
    line => "alias_maps = dbm:${::nrgillumos::postfix_config_directory}/aliases",
    require => [ Package["$::nrgillumos::postfix_pkg_name"], Exec['gcp_etc_aliases'] ],
    replace => true,
  }
  file_line { 'postfix_main.cf_alias_db':
    path => "${::nrgillumos::postfix_config_directory}/main.cf",
    match => '^alias_database = .*',
    line => "alias_database = dbm:${::nrgillumos::postfix_config_directory}/aliases",
    require => [ Package["$::nrgillumos::postfix_pkg_name"], Exec['gcp_etc_aliases'] ],
    replace => true,
  }
  file_line { 'postfix_aliases_comment':
    path => "${::nrgillumos::postfix_config_directory}/aliases",
    line => '# Managed by Puppet, please be careful about editing.',
    require => [ Package["$::nrgillumos::postfix_pkg_name"], Exec['gcp_etc_aliases'] ],
  }

  # Specify postfix relayhost parameter, if defined
  if $::nrgillumos::postfix_relay_host {  
    file_line { 'postfix_relayhost':
      path => "${::nrgillumos::postfix_config_directory}/main.cf",
      notify => Service[$::nrgillumos::postfix_service_name],
      line => "relayhost = ${::nrgillumos::postfix_relay_host}",
      match => '^relayhost = .*',
      replace => true,
    }
  }
  
  # Specify postfix root alias, if defined
  if $::nrgillumos::postfix_root_alias {
    file_line { 'postfix_root_alias':
      path => "${::nrgillumos::postfix_config_directory}/aliases",
      line => "root: ${::nrgillumos::postfix_root_alias}",
      match => '^root:.*',
      replace => true,
      require => [ Exec['gcp_etc_aliases'], File_line['postfix_main.cf_alias_maps'],
                   File_line['postfix_main.cf_alias_db'] ],
      notify => Exec['postfix_newaliases'],
    }
    exec { 'postfix_newaliases':
      command => '/opt/csw/bin/newaliases',
      refreshonly => true,
      notify => Service[$::nrgillumos::postfix_service_name],
    }
  }

  # Declare service resource, enabling if specified
  $postfix_ensure = $::nrgillumos::postfix_service_enabled ? {
    true => 'running',
    default => 'stopped',
  }
  service { $::nrgillumos::postfix_service_name:
    ensure => $postfix_ensure,
    enable => $::nrgillumos::postfix_service_enabled,
    require => Package[$::nrgillumos::postfix_pkg_name],
  }

  # Declare other file resources needed for postfix
  file {
    '/var/spool/postfix':
      ensure => directory,
      group => 'postdrop',
      require => Package["$::nrgillumos::postfix_pkg_name"];
    '/etc/postfix':
      target  => $::nrgillumos::postfix_config_directory,
      ensure  => link,
      require => Package["$::nrgillumos::postfix_pkg_name"];
    '/opt/csw/sbin/sendmail':
      ensure => file,
      mode => '755',
      require => Package["$::nrgillumos::postfix_pkg_name"];
    '/var/log/maillog':
      ensure => file,
      require => Package["$::nrgillumos::postfix_pkg_name"];
    '/root/Mail':
      ensure => directory;
    '/usr/lib/sendmail':
      ensure => link,
      target => '/opt/csw/sbin/sendmail',
      require => Package["$::nrgillumos::postfix_pkg_name"];
  }
  
}
