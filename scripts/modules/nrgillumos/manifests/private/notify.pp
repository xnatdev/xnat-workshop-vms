# This is the private class nrgillumos::private::notify, and it should not be
# included explicitly.
#
# This class manages SMF notificiations, and it is included by nrgillumos::init.
#
class nrgillumos::private::notify {
  
  # Declare package resource
  package { 'service/fault-management/smtp-notify':
    ensure => installed,
    notify => Service['system/fm/smtp-notify'],
  }
  
  # Declare service resource
  service {'system/fm/smtp-notify':
    ensure     => running,
    enable     => true,
  }
  
  # Now run any setnotify commands specified
  if ($::nrgillumos::setnotify_exec_data and $::nrgillumos::setnotify_exec_defaults) {
    create_resources('exec', $::nrgillumos::setnotify_exec_data, $::nrgillumos::setnotify_exec_defaults)
  }
  elsif ($::nrgillumos::setnotify_exec_data) {
    create_resources('exec', $::nrgillumos::setnotify_exec_data)
  }

}
