# Class: nrgillumos::private::opencsw_packages
# ===============================
#
# Private class to handle OpenCSW package installation for OmniOS.
#
# Parameters
# ----------
#
# Parameters for this module.
#
# NONE
#
# Variables
# ----------
#
# Variables used by this module.
#
# * `nrgillumos::opencsw_pkg_provider`
#  The package provider to use when installing OpenCSW packages.  Default: pkgutil.
#
# Examples
# --------
#
# @example
#    class { '::nrgillumos::private::opencsw_packages': }
#
# Authors
# -------
#
# Chip Schweiss <chip.schweiss@wustl.edu>
#
# Copyright
# ---------
#
# Copyright 2016 Chip Schweiss, unless otherwise noted.
#

class nrgillumos::private::opencsw_packages {
  
  $package_list = [ 'CSWsasl',
                    'CSWmutt',
                    'CSWgit',
                    'CSWtop',
                    'CSWiozone',
                    'CSWgbc',
                    'CSWlibnet1',
                    'CSWlibpcap1',
                    'CSWboost-gcc-dev',
                    'CSWnagios-plugins' ]

  # We might need these packages as alterantives to omnios versions
  # 'CSWcmake'
  # 'CSWgmake'
  # 'CSWgcc4g++'
  
  package {
    $package_list:
      ensure   => present,
      install_options => ['-y'],
      provider => $::nrgillumos::opencsw_pkg_provider;
    'CSWlibpcap':
      ensure  => present,
      provider => $::nrgillumos::opencsw_pkg_provider,
      require => File['/opt/csw/lib/libnet.so'];
  }
  
  file {
    '/opt/csw/lib/libnet.so':
      target  => 'libnet.so.1',
      ensure  => link,
      require => Package['CSWlibpcap1'];
  }
}
