# This is the private class nrgillumos::private::icinga2_client_install, and it should not be
# included explicitly.
#
# This class manages Icinga2 config, and it is included by nrgillumos::icinga2_client.
#
class nrgillumos::private::icinga2_client_config {

  require ::nrgillumos::private::icinga2_client_install

  $lower_fqdn = downcase($::fqdn) # Puppet has problems with mixed-case FQDN's
  $ld_library_path = '/usr/local/lib/:/opt/csw/gxx/lib'
  $pki_cert_path = "${::nrgillumos::icinga2_client::install_prefix}/etc/icinga2/pki/${lower_fqdn}.crt"
  $pki_cert_source = "${::settings::ssldir}/certs/${lower_fqdn}.pem"
  $pki_key_path = "${::nrgillumos::icinga2_client::install_prefix}/etc/icinga2/pki/${lower_fqdn}.key"
  $pki_key_source = "${::settings::ssldir}/private_keys/${lower_fqdn}.pem"
  $pki_ca_path = "${::nrgillumos::icinga2_client::install_prefix}/etc/icinga2/pki/ca.crt"
  $pki_ca_source = "${::settings::ssldir}/certs/ca.pem"
  $pki_crl_path = "${::nrgillumos::icinga2_client::install_prefix}/etc/icinga2/pki/crl.pem"
  $pki_crl_source = "${::settings::ssldir}/crl.pem"

  # File resources needed for config
  file_line {
    'icinga2_constants_comment':
      path => "${::nrgillumos::icinga2_client::install_prefix}/etc/icinga2/constants.conf",
      after => '^ \* the other configuration files.*$',
      line => ' * This file is managed by the nrg/nrgillumos Puppet module; please be careful editing.';
    'icinga2_constants_plugindir':
      path => "${::nrgillumos::icinga2_client::install_prefix}/etc/icinga2/constants.conf",
      match => '^const PluginDir =.*$',
      line => "const PluginDir = \"${::nrgillumos::icinga2_client::checkplugin_libdir}\"";
    'icinga2_constants_plugincontribdir':
      path => "${::nrgillumos::icinga2_client::install_prefix}/etc/icinga2/constants.conf",
      match => '^const PluginContribDir =.*$',
      line => "const PluginContribDir = \"${::nrgillumos::icinga2_client::checkplugin_libdir}\"";
    'icinga2_constants_manubulonplugindir':
      path => "${::nrgillumos::icinga2_client::install_prefix}/etc/icinga2/constants.conf",
      match => '^const ManubulonPluginDir =.*$',
      line => "const ManubulonPluginDir = \"${::nrgillumos::icinga2_client::checkplugin_libdir}\"";
  }

  file {
    "${::nrgillumos::icinga2_client::install_prefix}/etc/icinga2/zones.conf":
      ensure  => file,
      content => template('nrgillumos/icinga2/zones.conf.erb');
    "${::nrgillumos::icinga2_client::install_prefix}/etc/icinga2/conf.d/endpoints.conf":
      ensure  => file,
      content => template('nrgillumos/icinga2/endpoints.conf.erb');
    "${::nrgillumos::icinga2_client::install_prefix}/etc/icinga2/features-enabled/mainlog.conf":
      ensure => link,
      target => '../features-available/mainlog.conf';
    "${::nrgillumos::icinga2_client::install_prefix}/etc/icinga2/features-enabled/checker.conf":
      ensure => link,
      target => '../features-available/checker.conf';
    "${::nrgillumos::icinga2_client::install_prefix}/etc/icinga2/features-enabled/notification.conf":
      ensure => link,
      target => '../features-available/notification.conf';
    "${::nrgillumos::icinga2_client::install_prefix}/etc/icinga2/features-enabled/api.conf":
      ensure => link,
      target => '../features-available/api.conf';
    "${::nrgillumos::icinga2_client::install_prefix}/etc/icinga2/features-available/api.conf":
      ensure => file,
      content => template('nrgillumos/icinga2/api.conf.erb');
    'icinga2-pki-ca':
      ensure => file,
      path   => $pki_ca_path,
      source => $pki_ca_source;
    'icinga2-pki-cert':
      ensure => file,
      path   => $pki_cert_path,
      source => $pki_cert_source;
    'icinga2-pki-key':
      ensure => file,
      path   => $pki_key_path,
      source => $pki_key_source;
    'icinga2-pki-crl':
      ensure => file,
      path   => $pki_crl_path,
      source => $pki_crl_source;
    'icinga2_smf_method_script_solaris':
      path => '/lib/svc/method/icinga2',
      ensure => present,
      mode => '0755',
      owner => 'root',
      group => 'bin',
      content => template('nrgillumos/icinga2/svc-icinga2.erb');
    'icinga2_smf_manifest_solaris':
      path => '/lib/svc/manifest/site/icinga2.xml',
      ensure => present,
      content => template('nrgillumos/icinga2/smf-icinga2.xml.erb');
    # The default hosts.conf conflicts with our config
    "${::nrgillumos::icinga2_client::install_prefix}/etc/icinga2/conf.d/hosts.conf":
      ensure => absent;
  }

}
