# This is the private class nrgillumos::private::vim, and it should not be
# included explicitly.
#
# This class manages vim, and it is included by nrgillumos::init.
#
class nrgillumos::private::vim {
  
  # Add comments to config files managed
  file_line { "filetype.vim_comment":
    path  => "/usr/share/vim/vim${::nrgillumos::vim_version}/filetype.vim",
    line  => "\" Managed by Puppet, please be careful about editing.",
    after => '.*Vim support file to detect file types'
  }

  # Specify postfix relayhost parameter
  file_line { "filetype.vim_crontab":
    path => "/usr/share/vim/vim${::nrgillumos::vim_version}/filetype.vim",
    line => "au BufNewFile,BufRead crontab,crontab.*,crontab*,*/etc/cron.d/*     call s:StarSetf('crontab')",
    match => "^au BufNewFile,BufRead crontab.*",
    replace => true,
  }

  file {
    '/usr/share/vim/vimrc':
      mode    => '644',
      source  => "puppet:///modules/nrgillumos/vimrc",
      replace => true;
  }
  
}
