# Class: nrgillumos::private::packages
# ===============================
#
# Private class to handle package installation for OmniOS.
#
# Parameters
# ----------
#
# Parameters for this module.
#
# NONE
#
# Variables
# ----------
#
# NONE
#
# Examples
# --------
#
# @example
#    class { '::nrgillumos::private::packages': }
#
# Authors
# -------
#
# Chip Schweiss <chip.schweiss@wustl.edu>
#
# Copyright
# ---------
#
# Copyright 2016 Chip Schweiss, unless otherwise noted.
#

class nrgillumos::private::packages {
  
  $package_list = [ 'developer/library/lint',
                    'developer/parser/bison',
                    'developer/lexer/flex',
                    'system/header',
                    'developer/gcc48',
                    'developer/build/gnu-make',
                    'omniti/developer/build/cmake' ]

  package {
    $package_list:
      ensure   => present,
  }
}
