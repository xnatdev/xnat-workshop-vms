# This is the private class nrgillumos::private::icinga2_client_install, and it should not be
# included explicitly.
#
# This class manages Icinga2 installation, and it is included by nrgillumos::icinga2_client.
#
class nrgillumos::private::icinga2_client_install {

  include '::wget'

  $icinga2_wget_dest = '/opt/icinga2_puppet/' # Trailing slash is required for wget
  $icinga2_make_path = "${::nrgillumos::profile_path}:/opt/gcc-4.8.1/bin:/opt/omni/bin"
  $libboost_include = '/opt/csw/gxx/include/'
  $tarball_filename = basename($::nrgillumos::icinga2_client::icinga2_fetch_url)
 
  # Need this to coax maestrodev/wget to work under Solaris
  if (! defined(Package['wget']) ) {
    package { 'wget':
      ensure => present,
    }
  }

  # Retrieve the Icinga2 tarball package file, but only if not already downloaded
  wget::fetch { 'icinga2_tarball':
    source => $::nrgillumos::icinga2_client::icinga2_fetch_url,
    source_hash => $::nrgillumos::icinga2_client::icinga2_md5,
    destination => $icinga2_wget_dest,
    unless => "test -f ${icinga2_wget_dest}${tarball_filename}",
    notify => Exec['icinga2_untar'],
  }
  File[$icinga2_wget_dest] -> Wget::Fetch['icinga2_tarball']

  # File resources needed for install
  file {
    $icinga2_wget_dest:
      ensure => directory;
    "${icinga2_wget_dest}${tarball_filename}_unpacked":
      ensure => directory,
      require => File[$icinga2_wget_dest];
    "${icinga2_wget_dest}${tarball_filename}_unpacked/build":
      ensure => directory;
    '/etc/icinga2':
      ensure => link,
      target => "${::nrgillumos::icinga2_client::install_prefix}/etc/icinga2",
      require => Exec['icinga2_gmake_install'];
  }

  # Create icinga user and groups
  user { $::nrgillumos::icinga2_client::config_owner:
    home => $icinga2_wget_dest,
    gid => $::nrgillumos::icinga2_client::config_group,
    shell => '/usr/gnu/bin/false',
    require => Group["${::nrgillumos::icinga2_client::config_group}"];
  }
  group {
    $::nrgillumos::icinga2_client::config_group:
      ensure => present;
    $::nrgillumos::icinga2_client::command_group:
      members => [$::nrgillumos::icinga2_client::config_owner],
      ensure => present;
  }
  
  # Untar and build Icinga2 from source
  exec {
    # This assumes GNU tar and gzip are installed
    'icinga2_untar':
      command => "/usr/gnu/bin/tar xfz ${icinga2_wget_dest}${tarball_filename} --strip-components=1",
      cwd => "${icinga2_wget_dest}${tarball_filename}_unpacked",
      refreshonly => true,
      require => File["${icinga2_wget_dest}${tarball_filename}_unpacked"],
      notify => [ File["${icinga2_wget_dest}${tarball_filename}_unpacked/build"], Exec['icinga2_cmake'] ];
    'icinga2_cmake':
      command => "cmake -DICINGA2_WITH_MYSQL=OFF -DICINGA2_WITH_PGSQL=OFF -DBOOST_INCLUDEDIR=${libboost_include} -DICINGA2_COMMAND_GROUP=${::nrgillumos::icinga2_client::command_group} -DCMAKE_INSTALL_PREFIX=${::nrgillumos::icinga2_client::install_prefix} ..",
      path => $icinga2_make_path,
      cwd => "${icinga2_wget_dest}${tarball_filename}_unpacked/build",
      refreshonly => true,
      notify => Exec['icinga2_gmake'];
    'icinga2_gmake':
      command => 'gmake',
      path => $icinga2_make_path,
      cwd => "${icinga2_wget_dest}${tarball_filename}_unpacked/build",
      timeout => 1500,
      refreshonly => true,
      notify => Exec['icinga2_gmake_install'];
    'icinga2_gmake_install':
      command => 'gmake install',
      path => $icinga2_make_path,
      cwd => "${icinga2_wget_dest}${tarball_filename}_unpacked/build",
      refreshonly => true,
      notify => Exec['icinga2_chown'];
    'icinga2_chown':
      command => "chown -R ${::nrgillumos::icinga2_client::config_owner}.${::nrgillumos::icinga2_client::config_group} ${::nrgillumos::icinga2_client::install_prefix}/var/{log/icinga2,cache/icinga2,lib/icinga2,run/icinga2}",
      refreshonly => true;
  }

}
