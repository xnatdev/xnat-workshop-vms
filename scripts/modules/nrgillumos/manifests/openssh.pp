# Class: nrgillumos::ssh
# ===========================
#
# This class is a simple wrapper around the ghoneycutt/ssh module, to manage reliable removal
# of existing SSH (i.e. SunSSH) followed by installation of OpenSSH as replacement.  These
# tasks are collected together in the class below to ensure coherent dependency management,
# in particular to avoid situations where a failed puppet agent run leaves a machine devoid of
# any SSH client at all.
#
# This class will invoke the ghoneycutt/ssh module, with the expectation additional params
# for ghoneycutt/ssh are supplied via hiera.
#
# Parameters
# ----------
#
# Below are the parameters this class accepts
#
# * `remove_packages`
#  An array of existing SSH packages to remove prior to installing OpenSSH.  Note that full
#  FMRIs seem to be necessary.  Default value: [ 'pkg://omnios/network/ssh', 
#  'pkg://omnios/network/ssh/ssh-key', 'pkg://omnios/service/network/ssh' ].
#
# * `install_packages`
#  An array of OpenSSH packages to pass to ghoneycutt/ssh module for installation, default is 
#  [ 'pkg://omnios/network/openssh', 'pkg://omnios/network/openssh-server' ] .
#
# Variables
# ----------
#
# Custom variables that this class requires.
#
# NONE
#
# Examples
# --------
#
# @example
#    class { '::nrgillumos::openssh':
#      remove_packages = [ 'pkg://omnios/network/ssh', 'pkg://omnios/network/ssh/ssh-key' ],
#    }
#
# Authors
# -------
#
# Author Name <author@domain.com>
#
# Copyright
# ---------
#
# Copyright 2015 Your name here, unless otherwise noted.
#
class nrgillumos::openssh (
  Array $remove_packages =  [ 'pkg://omnios/network/ssh', 'pkg://omnios/network/ssh/ssh-key', 'pkg://omnios/service/network/ssh' ],
  Array $install_packages =  [ 'pkg://omnios/network/openssh', 'pkg://omnios/network/openssh-server' ],
  ) {
  
  # Include base class
  include ::nrgillumos

  # Remove existing SSH packages specified, while also installing new ones in place.  Have to use
  # exec since can't run pkg uninstall on these packages singly.
  $remove_packages_reject = join(regsubst($remove_packages, '^(.*)$', '--reject \1'), " ")
  $remove_packages_pkg_list = join($remove_packages, " ")
  $install_packages_real = join($install_packages, " ")
  exec { 'replace_old_ssh':
    command => "/usr/bin/pkg install ${remove_packages_reject} ${install_packages_real}",
    onlyif => "/usr/bin/pkg list -Hv ${remove_packages_pkg_list} 2>&1 | grep 'i--'",
  }
  ->
  # Now invoke ghoneycutt/ssh module
  class { '::ssh':
    packages => $install_packages,
    sshd_config_print_motd => "no" # Otherwise double MOTD displayed
  }  
}
