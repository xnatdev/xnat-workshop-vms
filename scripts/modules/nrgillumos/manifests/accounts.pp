# Class: nrgillumos::accounts
# =============================
#
# This class manages custom user account creation tasks on NRG OmniOS/Illumos machines.
# That is, this class is intended to be a wrapper around the deric/accounts module, e.g.
# to perform custom ZFS home directory creation steps that would need to be done before
# actually calling "useradd".
#
# TODO: Review once we're at OmniOS r151016, where userdel is ZFS aware
# Ref: http://omnios.omniti.com/wiki.php/ReleaseNotes/r151016
#
# Parameters
# ----------
#
# Below are the parameters this class accepts
#
# * `users`
#  A hash of users to manage, identical to the format expected by deric/accounts module
#  (which this class will pass to that module).  Unless the user's homedir arleady exists,
#  this module will also create an individual ZFS filesystem for that user's home directory.
#
# * `groups`
#  A hash of user groups, identical to the format expected by deric/accounts module (which
#  this class will pass to that module).
#
# Variables
# ----------
#
#  NONE
#
# Examples
# --------
#
# @example
#    class { '::nrgillumos::accounts':
#      users => {
#        'fred',
#        'charlie',
#      }
#    }
#
# Authors
# -------
#
# Author Name <author@domain.com>
#
# Copyright
# ---------
#
# Copyright 2015 Your name here, unless otherwise noted.
#
class nrgillumos::accounts (
  Optional[Hash] $users = undef,
  Optional[Hash] $groups = undef,
) {

  # Include base class and deric/accounts
  include ::nrgillumos
  class { '::accounts':
    users => $users,
    groups => $groups,
  }

  # Loop through users retrieved from hiera
  $users.each |$uname, $user_hash| {

    if ($user_hash['ensure'] != 'absent') {

      # Create home directory on zfs pool, but only if user homedir doesn't already exist
      exec { "create_zfs_home_${uname}":
        command => "zfs create rpool/export/home/${uname}",
        unless => [ "zfs list rpool/export/home/${uname} 2>/dev/null", 
                    "test -d /export/home/${uname}" ]
      }

      # Append to /etc/auto_home
      file_line { "auto_home_${uname}":
        path => '/etc/auto_home',
        line => "${uname}    localhost:/export/home/&",
        match => "^${uname}.*",
        replace => true,
        require => Exec["create_zfs_home_${uname}"],
      }

      # Now invoke deric/accounts, e.g. run useradd
      contain ::accounts

      # OmniOS locks user accounts w/ no password, so unlock them
      exec { "passwd_unlock_${uname}":
        command => "passwd -d ${uname}",
        onlyif => "test `passwd -s ${uname} | gawk '{print \$2}'` == 'LK'",
        require => User["${uname}"],
      }

    }
    # Otherwise remove a user
    else {

      # Remove from /etc/auto_home
      file_line { "remove_auto_home_${uname}":
        path => '/etc/auto_home',
        ensure => 'absent',
        line => "${uname}    localhost:/export/home/&",
        match => "^${uname}.*",
      }

      # Kludgy hacks to redirect user to temporary home dir so userdel doesn't fail below
      # TODO: Review once we're at OmniOS r151016, where userdel is ZFS aware
      # Ref: http://omnios.omniti.com/wiki.php/ReleaseNotes/r151016
      exec { "temp_homedir_${uname}":
        command => "mkdir -p /tmp/temp_homedir_${uname}",
      }
      ->
      exec { "chown_homedir_${uname}":
        command => "chown -R ${uname} /tmp/temp_homedir_${uname}",
      }
      ->
      exec { "usermod_homedir_${uname}":
        command => "usermod -d /tmp/temp_homedir_${uname} ${uname}",
      }
      ->
      exec { "destroy_zfs_home_${uname}":
        command => "zfs destroy -f rpool/export/home/${uname}",
        onlyif => "zfs list rpool/export/home/${uname} 2>/dev/null",
      }

      # Now invoke deric/accounts, e.g. run userdel
      contain ::accounts

    }
  }
}


