#!/bin/bash
#
# XNAT Vagrant provisioning script for OmniOS / ZFS demostration
# http://www.xnat.org
# Copyright (c) 2016, Washington University School of Medicine, all rights reserved.
# Released under the Simplified BSD license.
#

echo Now running the "provision.sh" provisioning script.

sourceScript() {
    test -f /vagrant/scripts/$1 && source /vagrant/scripts/$1 || source /vagrant-root/scripts/$1
}

# Now initialize the build environment from the config's vars.sh settings.
source /vagrant/.work/vars.sh

# look in config's scripts folder first, then try the multi root
sourceScript macros
sourceScript defaults.sh

# Overide some params specific to OmniOS
PUPPET_BINDIR="/opt/csw/bin"

# Ruby Gems update from: https://github.com/rubygems/rubygems/releases
RUBYGEMS_VERSION="2.0.15"
RUBYGEMS_UPDATE="rubygems-update-${RUBYGEMS_VERSION}.gem"
RUBYGEMS_UPDATE_SOURCE="https://github.com/rubygems/rubygems/releases/download/v${RUBYGEMS_VERSION}"

# Install OpenCSW.
installpackage() {
    package="$1"
    /usr/bin/pkginfo|grep -q $package &>/dev/null
    if [ "$?" -ne "0" ]; then
        echo "Installing $package"
        /opt/csw/bin/pkgutil -y -i $package
    else
        echo "Package $package already installed.  Skipping."
    fi
}

pkginfo CSWpkgutil 2> /dev/null
if [ "$?" -ne "0" ]; then
    cat > /tmp/pkgadd_opencsw <<PKGADD
mailinstance=overwrite
partial=nocheck
runlevel=nocheck
idepend=nocheck
rdepend=nocheck
space=nocheck
setuid=nocheck
conflict=nocheck
action=nocheck
basedir=default
PKGADD
    echo "all\n\n" | pkgadd -d http://get.opencsw.org/now -a /tmp/pkgadd_opencsw
fi
pkg install archiver/gnu-tar

# Install some pre-reqs
installpackage cswpki
installpackage CSWcoreutils
installpackage CSWruby20

# Update rubygems
current_gems_version=`/opt/csw/bin/gem --version`

if [ "$current_gems_version" != "${RUBYGEMS_VERSION}" ]; then
    wget ${RUBYGEMS_UPDATE_SOURCE}/${RUBYGEMS_UPDATE}
    /opt/csw/bin/gem install --local ${RUBYGEMS_UPDATE} && rm ${RUBYGEMS_UPDATE}
fi

# Download and install puppet-agent if not already present
if [ ! -f "/opt/csw/bin/puppet" ]; then
    /opt/csw/bin/gem install --no-rdoc --no-ri puppet
fi

# Install any additional specified packages
if [ -v INSTALL ]; then
    echo "Installing additional packages: ${INSTALL}"
    installpackage ${INSTALL}
fi

# Now run puppet
PATH=${PATH}:/usr/local/bin:/opt/csw/bin /opt/csw/bin/puppet apply --verbose --modulepath=/vagrant-root/scripts/modules /vagrant/omnios-zfs.pp | tee -a /var/log/puppet_deploy.log

## Set up Docker to listen for external connections
#echo ""
#echo "Creating Docker service configuration file"
#sudo mkdir /etc/systemd/system/docker.service.d
#replaceTokens docker.conf | sudo tee /etc/systemd/system/docker.service.d/docker.conf


## Setup Docker RemoteAPI
#echo ""
#echo "Opening access to port 2375 and docker.sock for Docker RemoteAPI"
#echo "DOCKER_OPTS='-H tcp://0.0.0.0:2375 -H tcp://0.0.0.0:2376 -H unix:///var/run/docker.sock'" | sudo tee -a /etc/default/docker

