#!/bin/bash

#
# Build script to support OmniOS / ZFS demo
#

echo Now running the "build-omnios.sh" provisioning script. I don't deploy XNAT to this OmniOS machine. Please diregard message above.

sourceScript() {
    test -f /vagrant/scripts/$1 && source /vagrant/scripts/$1 || source /vagrant-root/scripts/$1
}

# Now initialize the build environment from the config's vars.sh settings.
source /vagrant/.work/vars.sh

# look in config's scripts folder first, then try the multi root
sourceScript macros
#sourceScript defaults.sh

# Configure the host settings.
echo -e "${VM_IP} ${HOST} ${SERVER}" | sudo tee --append /etc/hosts

# I don't actually do much for a 'build' script.

STATUS=$?
if [[ ${STATUS} == 0 ]]; then
    # after setup is successful, specify 'reload' as the startup command
    printf "reload" > /vagrant/.work/startup
    echo "==========================================================="
    echo "Your OmniOS VM's IP address is ${VM_IP} and your deployed "
    echo "==========================================================="
    exit 0;
else
    echo The application does not appear to have started properly. Status code: ${STATUS}
fi

exit ${STATUS}

